angular.module('myApp.interviewsApp', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/allInterviews', {
    templateUrl: 'allInterviews/allInterviews.html',
    controller: 'allInterviewsController'
  });
}])

.controller('allInterviewsController', function ($scope, $http, $sce) {
  $http.get('http://127.0.1.1/mediacloud/interviewsAll.php').success(function(data) {
    $scope.interviewVideos = data;
  });
  
  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }

  $scope.orderProp = 'title';
});
