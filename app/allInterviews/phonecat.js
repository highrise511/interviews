var phonecatApp = angular.module('myApp.PhonecatApp', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/phonecatApp', {
    templateUrl: 'allInterviews/allInterviews.html',
    controller: 'PhoneListCtrl'
  });
}])

phonecatApp.controller('PhoneListCtrl', function ($scope, $http) {
  $http.get('phones/phones.json').success(function(data) {
    $scope.phones = data;
  });

  $scope.orderProp = 'age';
});
